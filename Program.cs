﻿using System;
using System.Collections.Generic;

namespace POO
{
    class Program
    {
        static void Main(string[] args)
        {
            Bus bus = new Bus(100, 60);
            Routes routes = new Routes(2, bus);
            Driver driver = new Driver("Mr", "Driver", bus);
            driver.toString();
            routes.Stations.Add(new Station("S1"));
            routes.Stations.Add(new Station("S2"));
            routes.Stations.Add(new Station("S3"));
            routes.toString();
            bus.Boarding(new Passenger(66));
        }
    }

    public class Station
    {
        private string Name;

        public Station(string name)
        {
            Name = name;
        }

        public string GetName()
        {
            return this.Name;
        }
    }

    public class Routes
    {
        private int Number;
        private Vehicles vehicle;
        public List<Station> Stations;

        public Routes(int number, Vehicles vehicles)
        {
            Number = number;
            vehicle = vehicles;
            Stations = new List<Station>();
        }

        public void toString()
        {
            Console.WriteLine("The route " + this.Number + " has the vehicle " + this.vehicle + " goes to the stations:");
            foreach (var VARIABLE in Stations)
            {
                Console.WriteLine(VARIABLE.GetName());
            }
        }
    }

    public abstract class Vehicles
    {
        protected int RegisterNumber;
        protected int Capacity;
        protected int CurrentCap = 0;
        protected Passenger[] passengers;

        public abstract void toString();
        public abstract void Boarding(Passenger passenger);

    }

    public class Tram : Vehicles
    {
        public Tram(int rnr, int cap)
        {
            Capacity = cap;
            RegisterNumber = rnr;
            passengers = new Passenger[Capacity];
        }

        public override void Boarding(Passenger passenger)
        {
            if (CurrentCap < Capacity)
            {
                passengers[CurrentCap] = passenger;
                CurrentCap++;
                passenger.toString();
            }
        }

        public override void toString()
        {
            Console.WriteLine("Tram " + this.RegisterNumber + "  with the capacity of " + this.Capacity + " goes on railroads");
        }
    }

    public class Bus : Vehicles
    {
        public Bus(int rnr, int cap)
        {
            Capacity = cap;
            RegisterNumber = rnr;
            passengers = new Passenger[Capacity];
        }

        public override void Boarding(Passenger passenger)
        {
            if (CurrentCap < Capacity)
            {
                passengers[CurrentCap] = passenger;
                CurrentCap++;
                passenger.toString();
            }
        }

        public override void toString()
        {
            Console.WriteLine("Bus " + this.RegisterNumber + " with the capacity of " + this.Capacity + " goes on roads");
        }
    }

    public interface IPeople
    {
        public void toString();
    }

    public class Driver : IPeople
    {
        private string FirstName;
        private string LastName;
        private Vehicles vehicle;

        public Driver(string fn, string ln, Vehicles vehicles)
        {
            FirstName = fn;
            LastName = ln;
            vehicle = vehicles;
        }

        public void toString()
        {
            Console.Write("Driver " + this.FirstName + " " + this.LastName + " drives ");
            this.vehicle.toString();
        }
    }

    public class TicketCollector : IPeople
    {
        private string FirstName;
        private string LastName;
        private Vehicles vehicle;

        public TicketCollector(string fn, string ln, Vehicles vehicles)
        {
            FirstName = fn;
            LastName = ln;
            vehicle = vehicles;
        }

        public void toString()
        {
            Console.Write("Ticket Collector " + this.FirstName + " " + this.LastName + " works on ");
            this.vehicle.toString();
        }
    }

    public class Passenger : IPeople
    {
        private int OrderNumber;

        public Passenger(int orderNumber)
        {
            OrderNumber = orderNumber;
        }
        public void toString()
        {
            Console.WriteLine("The Passenger " + this.OrderNumber + " is on.");
        }
    }
}
